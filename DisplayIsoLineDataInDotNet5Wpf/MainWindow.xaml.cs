﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows;
using ThinkGeo.Core;

namespace DisplayIsoLineDataInDotNet5Wpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Map_Loaded(object sender, RoutedEventArgs e)
        {
            // Create background world map with vector tile requested from ThinkGeo Cloud Service. 
            ThinkGeoCloudVectorMapsOverlay thinkGeoCloudVectorMapsOverlay = new ThinkGeoCloudVectorMapsOverlay("itZGOI8oafZwmtxP-XGiMvfWJPPc-dX35DmESmLlQIU~", "bcaCzPpmOG6le2pUz5EAaEKYI-KSMny_WxEAe7gMNQgGeN9sqL12OA~~", ThinkGeoCloudVectorMapsMapType.Light);
            map.Overlays.Add(thinkGeoCloudVectorMapsOverlay);

            // Create a new overlay that will hold our new layer and add it to the map.
            LayerOverlay isoLineOverlay = new LayerOverlay();
            map.Overlays.Add("isoLineOverlay", isoLineOverlay);

            // Create the layer based on the csv file with the mosquito data that we will use for the iso line.
            DynamicIsoLineLayer isoLineLayer = GetDynamicIsoLineLayer(@"../../data/Csv/Frisco_Mosquitos.csv");
            isoLineOverlay.Layers.Add("IsoLineLayer", isoLineLayer);

            // This is the extent of Frisco TX in the projection of SphericalMercator.
            map.CurrentExtent = new RectangleShape(-10793460, 3922456, -10763497, 3906748);

            // Refresh the map.
            map.Refresh();
        }

        private static Dictionary<PointShape, double> GetDataFromCSV(string csvFilePath)
        {
            // This code just reads the csv file into a dictionary of point shapes for the locations and mosquito population at those points.
            Dictionary<PointShape, double> csvDataPoints = new Dictionary<PointShape, double>();

            using (StreamReader streamReader = new StreamReader(csvFilePath))
            {
                string headline = streamReader.ReadLine();
                while (!streamReader.EndOfStream)
                {
                    string line = streamReader.ReadLine();
                    string[] parts = line.Split(',');
                    csvDataPoints.Add(new PointShape(double.Parse(parts[0]), double.Parse(parts[1])), double.Parse(parts[2]));
                }
            }
            return csvDataPoints;
        }

        private DynamicIsoLineLayer GetDynamicIsoLineLayer(string csvFilePath)
        {
            Dictionary<PointShape, double> csvPointData = GetDataFromCSV(csvFilePath);

            // We use this method to generate the values for the lines based on the data values and how many breaks we want.
            Collection<double> isoLineLevels = GridIsoLineLayer.GetIsoLineLevels(csvPointData, 25);

            //Create the new dynamicIsoLineLayer using the csvPointData Dictionary we created above, the collection of isoLineLevels, the GridInterpolationModel, and the IsoLineType. 
            DynamicIsoLineLayer dynamicIsoLineLayer = new DynamicIsoLineLayer(csvPointData, isoLineLevels, new InverseDistanceWeightedGridInterpolationModel(), IsoLineType.LinesOnly);

            // Here we add the line style to dynamicIsoLineLayer and color it blue. 
            dynamicIsoLineLayer.CustomStyles.Add(new LineStyle(GeoPens.Blue));

            //Create the text styles to label the lines and add it to the iso line layer.
            TextStyle textStyle = TextStyle.CreateSimpleTextStyle(dynamicIsoLineLayer.DataValueColumnName, "Arial", 12, DrawingFontStyles.Bold, GeoColors.Black, 0, 0);
            textStyle.HaloPen = GeoPens.White;
            textStyle.SplineType = SplineType.StandardSplining;
            dynamicIsoLineLayer.CustomStyles.Add(textStyle);

            return dynamicIsoLineLayer;
        }

        public void Dispose()
        {
            // Dispose of unmanaged resources.
            map.Dispose();
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }

    }
}
