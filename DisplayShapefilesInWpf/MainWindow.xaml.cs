﻿using System.Windows;
using ThinkGeo.Core;

namespace DisplayShapefilesInWpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Map_Loaded(object sender, RoutedEventArgs e)
        {
            // Create a shapefileLayer, passing in the path of a shape file. 
            ShapeFileFeatureLayer shapefileLayer = new ShapeFileFeatureLayer("Data/ne_110m_land.shp");

            // Set the default area style and apply it to all 20 ZoomLevels
            shapefileLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = new AreaStyle(GeoPens.Black);
            shapefileLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;

            // Add the layer to the ShapefileOverlay
            LayerOverlay overlay = new LayerOverlay();
            overlay.Layers.Add(shapefileLayer);
            map.Overlays.Add(overlay);

            // Set the map's extent to the bounding box of the shapefile
            ShapeFileFeatureLayer.BuildIndexFile("Data/ne_110m_land.shp");
            shapefileLayer.Open();
            map.CurrentExtent = shapefileLayer.GetBoundingBox();
            map.Refresh();
        }
    }
}
