﻿using System.Windows;
using ThinkGeo.Core;

namespace DisplaySqliteInWpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Map_Loaded(object sender, RoutedEventArgs e)
        {
            // Here we create SQLite feature layer.
            SqliteFeatureLayer sqlitLayer = new SqliteFeatureLayer(@"Data Source=Data\ne_110m_land.sqlite", "ne_110m_land", "id", "geometry");

            // Assign the style colors.
            sqlitLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = new AreaStyle(GeoPens.Black);
            // Apply the style across all zoom levels.
            sqlitLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;

            // Create an Overlay to store the layer and add it to the map.
            LayerOverlay landOverlay = new LayerOverlay();
            map.Overlays.Add(landOverlay);
            landOverlay.Layers.Add(sqlitLayer);

            // Set the map's extent to the bounding box of the sqlitLayer
            sqlitLayer.Open();
            map.CurrentExtent = sqlitLayer.GetBoundingBox();
            map.Refresh();
        }
    }
}
